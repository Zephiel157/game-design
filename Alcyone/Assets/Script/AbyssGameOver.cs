﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //Allows the unity to manage scenes
//The public class
public class AbyssGameOver : MonoBehaviour {
	//Serialized the load level so the level that is loaded can be edited
	[SerializeField] private string loadLevel;
	//The boz collider is a trigger
	void OnTriggerEnter2D(Collider2D other)
	{	//When the player box collider enters the Abyss box collider
		if (other.CompareTag("Player"))

		{	//Debug.log will show the message "GameOver" when the script runs
			Debug.Log ("GameOver!");
			//Loads the Game Over Scene
			SceneManager.LoadScene ("GameOver2");
		}

	}
}
