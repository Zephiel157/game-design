﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //Allows the unity to manage scenes

public class Restart : MonoBehaviour {

	//the public void for the restart button
	public void PlayGame(){
		//Activates/retrieves the scene "Restarts level"
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex - 4);
}
}