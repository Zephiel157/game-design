﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //Allows the unity to manage scenes

public class Boss : MonoBehaviour {

	//plays the sounbclip of the player jumping
	[SerializeField]
	public AudioClip die;

	//destroys monster when hit by bullet
	void OnCollisionEnter2D(Collision2D c){
		//if statement for bullet
		if (c.gameObject.tag.Equals ("Bullet")) {

			// Play a die sound
			AudioSource.PlayClipAtPoint (die, Camera.main.transform.position);

			// Destroy the bullet
			Destroy (c.gameObject);
			// Destroy the monster
			Destroy (gameObject);




		}
	}
	//Game over for player if touch
	void OnTriggerEnter2D(Collider2D other)
	{	//If the other collider that enters the enemy has the tag "Player"
		if (other.CompareTag("Player"))

		{	//Load the "Game Over" scene.
			Debug.Log ("GameOver!");
			SceneManager.LoadScene ("GameOver");
		}

	}

}
