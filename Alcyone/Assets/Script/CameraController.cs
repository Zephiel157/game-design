﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	//retrieves the public game object for the player
	public GameObject Player;
	//sets the camera position
	private Vector3 offset;

	// Use this for initialization
	void Start () {
		//fixes the camera to follow the players position
		offset = transform.position - Player.transform.position;

	}

	// Update is called once per frame
	void LateUpdate () {
		//transform position to player position
		transform.position = Player.transform.position + offset;

	}
}

