﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Start Program
public class EnemyPatrol : MonoBehaviour {
	//Enemy float speed and distance
	public float speed;
	public float distance;
	//Makes the enemy moving left statement true
	private bool movingLeft = true;
	//transform position upon point detection
	public Transform pointDetection;
	//checks for update
	void Update(){
		//moves the enemy left at a specified speed
		transform.Translate (Vector2.left * speed * Time.deltaTime);
		//uses the Racast for movement
		RaycastHit2D pointInfo = Physics2D.Raycast (pointDetection.position, Vector2.down, distance);
		//if statement for when the pointer is detected.
		if(pointInfo.collider == true){
			//if statement if moving left is fals
			if(movingLeft == false){
				//move right
				transform.eulerAngles = new Vector3 (0, 180, 0);
				movingLeft = true;
			}
			else{//else statement, moving left is false.
				transform.eulerAngles = new Vector3 (0, 0, 0);
				movingLeft = false;
			}
		}
	}
}
