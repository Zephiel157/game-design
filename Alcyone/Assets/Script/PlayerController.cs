﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //Allows the unity to manage scenes
//The player controller
public class PlayerController : MonoBehaviour {
	//The rigidbody to give the player physics
	private Rigidbody2D myRigidBody;
	//Attaches the sprite animations to the player GameObject
	private Animator myAnimator;
	//Allows the editing of the private field speed to modify player speed
	//Defines the Gun gameobject
	[SerializeField]
	private Transform Gun;
	//Plays the soundclip of the gun firing
	[SerializeField]
	public AudioClip pewPew;
	//plays the sounbclip of the player jumping
	[SerializeField]
	public AudioClip boing;

	//plays the sounbclip of the player jumping
	[SerializeField]
	public AudioClip death;

	//The serialized field to edit/load the level
	[SerializeField] private string loadLevel;

	[SerializeField]
	private float speed; //Gives the player speed
	//defines the player attack
	private bool attack;
	//defines the player starts facing right
	private bool facingRight;
	//defines the ground points and serializes them
	[SerializeField]
	private Transform[] groundPoints;
	//defines the ground radius and serializes them
	[SerializeField]
	private float groundRadius;
	//defines the layermask and "whatIsGround"
	[SerializeField]
	private LayerMask whatIsGround;

	//The private bool for isGrounded
	private bool isGrounded;
	//the private bool for jump
	private bool jump;
	//defines the float function for "jumpForce" and serializes it
	//so that the user can edit how high the player jumps
	[SerializeField]
	private float jumpForce;

	[SerializeField]
	private GameObject BulletPrefab;

	// Use this for initialization
	void Start () {
		//Starts the game with the player facing right
		//Loads up the player rigidbody and animations
		facingRight = true;
		myRigidBody = GetComponent<Rigidbody2D> ();
		myAnimator = GetComponent<Animator> ();
		
	}
	//the updat
	void Update(){
		//Checks constantly/each update for player inputs
		HandleInput ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//gives the player horizontal movement
		float horizontal = Input.GetAxis ("Horizontal");
		//Movement on the X-plane

		//redefines isGrounded
		isGrounded = IsGrounded ();
		//The horizontal movement
		Movement (horizontal);
		//checks if needs to flip sprite
		//Flips the sprite when moving the other direction
		Flip (horizontal);
		//Checks for player attack
		Attacks ();

		HandleLayers ();

		//Checks to reset
		ResetValues ();
		
	}
	//Movement
	private void Movement(float horizontal){
		//animation for shooting
		if (!this.myAnimator.GetCurrentAnimatorStateInfo (0).IsTag ("Shooting")) {
			//the calculations for the physics for the RigidBody2D
			myRigidBody.velocity = new Vector2 (horizontal * speed, myRigidBody.velocity.y);
		}
		//The if statement for the isGrounded and jump
		if (isGrounded && jump) {
			//checks to see if the isGrounded function is false
			isGrounded = false;
			myRigidBody.AddForce(new Vector2 (0, jumpForce));
			Debug.Log ("isGrounded");
			myAnimator.SetTrigger ("Jump");
		}

		//Links the animation from the animator to the horizontal movement
		myAnimator.SetFloat ("movementSpeed",Mathf.Abs(horizontal));


	}
	//The function for the player attack.
	private void Attacks(){
		//If statement for the attack animation and action
		if (attack && !this.myAnimator.GetCurrentAnimatorStateInfo (0).IsTag ("Shooting")) {
			//sets the animation loop as a trigger for the user input
			myAnimator.SetTrigger ("attack");
			myRigidBody.velocity = Vector2.zero;

		}
	}
	//the private void for the user input
	private void HandleInput(){
		//if statement for when the user presses the spacebar key
		if (Input.GetKeyDown (KeyCode.Space)) {
			// Play a shoot sound
			AudioSource.PlayClipAtPoint (boing, Camera.main.transform.position);
			//sets jump equal to true
			jump = true; 
		}
		//if statement for when the user presses the left shift key
		if (Input.GetKeyDown (KeyCode.LeftShift)) {
			// Play a shoot sound
			AudioSource.PlayClipAtPoint (pewPew, Camera.main.transform.position);
			//sets attack to true
			attack = true;
			Shoot (0);
		}

	}
	//The private void for the float function for the horizontal movement
	//and the direction the sprite is facing
	private void Flip(float horizontal){
		//if statement for the horizontal, sprite direction
		if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight) {
			//facingRight 
			facingRight = !facingRight;
			//transforms the scale of the object
			Vector3 theScale = transform.localScale;
			//tye scale is equal to -1
			theScale.x *= -1;
			//transforms the scale of the sprite
			transform.localScale = theScale;
		}
	}
	//The private void that resets the values for the attack and jump animations and functions
	private void ResetValues(){

		attack = false;
		jump = false;
	}
	//The private bool for the IsGrounded function 
	private bool IsGrounded(){
		//If statement for the RigidBody2D of the game object
		if (myRigidBody.velocity.y <= 0) {
			//foreach statment for the groundpoints
			foreach (Transform point in groundPoints) {
				//determines of the groundpoint colliders pverlap the ground colliders
				Collider2D[] colliders = Physics2D.OverlapCircleAll (point.position, groundRadius, whatIsGround);
				//the colliders radius length
				for (int i = 0; i < colliders.Length; i++) {
					//if statement for the two colliders overlapping
					if (colliders[i].gameObject != gameObject) {
						//return true if they are
						return true;
					}
				}
			}
		}
		//otherwise retrun false
		return false;
	}
	//handles the layers for the animator between jumping and grounded
	private void HandleLayers(){

		if (!isGrounded) {

			myAnimator.SetLayerWeight (1, 1);
		}

		else {

			myAnimator.SetLayerWeight (1, 0);
		}
	}
	//transforms the position and direction of the bullets when they are fired
	//in relation to the player position
	public void Shoot(int value){

		if (!isGrounded && value == 1 || isGrounded && value == 0){

			//If statement for when the player is facing right
			if (facingRight) {

				GameObject tmp = (GameObject)Instantiate (BulletPrefab, Gun.position, Quaternion.Euler (new Vector3 (0, 0, 0)));
				tmp.GetComponent<BulletScript> ().Initialize (Vector2.right);
			} else { //else if they are facing left

				GameObject tmp = (GameObject)Instantiate (BulletPrefab, Gun.position, Quaternion.Euler (new Vector3 (0, 0, 180)));
				tmp.GetComponent<BulletScript> ().Initialize (Vector2.left);
		
			}
		}
	}
	void OnCollisionEnter2D(Collision2D c){

		if (c.gameObject.tag.Equals ("Enemy")) {

			// Play a die sound
			AudioSource.PlayClipAtPoint (death, Camera.main.transform.position);

			Debug.Log ("GameOver!");
			//Loads the Game Over Scene
			SceneManager.LoadScene ("GameOver");


		}
	}
}
	