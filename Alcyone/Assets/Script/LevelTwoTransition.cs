﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //Allows the unity to manage scenes
//The public class for level two transition
public class LevelTwoTransition : MonoBehaviour {
	//The serialized field to edit/load the level
	[SerializeField] private string loadLevel;
	//The trigger for when the player box collider enters
	void OnTriggerEnter2D(Collider2D other)
	{	//if statement for the player tag
		if (other.CompareTag("Player"))

		{	//Debug to print message when program runs
			//loads the Level 1.2
			Debug.Log ("Level 1.2");
			SceneManager.LoadScene ("Level 1.2");
		}

	}
}
