﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform2 : MonoBehaviour {

	//Enemy float speed and distance
	public float speed;
	public float distance;
	//Makes the platform moving up statement true
	private bool movingUp = true;
	//transform position upon point detection
	public Transform pointDetection;
	//checks for update
	void Update(){
		//moves the enemy left at a specified speed
		transform.Translate (Vector2.up * speed * Time.deltaTime);
		//uses the Racast for movement
		RaycastHit2D pointInfo = Physics2D.Raycast (pointDetection.position, Vector2.down, distance);
		//if statement for when the pointer is detected.
		if(pointInfo.collider == true){
			//if statement if moving up is false
			if(movingUp == false){
				//move up
				transform.eulerAngles = new Vector3 (0, 0, 90);
				movingUp = true;
			}
			else{//else statement, moving up is false.
				transform.eulerAngles = new Vector3 (0, 0, 0);
				movingUp = false;
			}
		}
	}
}
