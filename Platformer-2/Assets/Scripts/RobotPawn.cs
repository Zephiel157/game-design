﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotPawn : Pawn {

	private Rigidbody2D rb;
	private SpriteRenderer sr;
	public float moveSpeed;
	public float jumpForce;

	public void Start() {
		// Get my components
		rb = GetComponent<Rigidbody2D>();
		sr = GetComponent<SpriteRenderer> ();
	}

	public override void Move (Vector2 moveVector) {
		rb.velocity = new Vector2 (moveVector.x * moveSpeed, rb.velocity.y);

		if (rb.velocity.x < 0) {
			sr.flipX = true;
		} else {
			sr.flipX = false;
		}
	}

	public override void Jump () {
		rb.AddForce (Vector2.up * jumpForce, ForceMode2D.Force);
	}

}
