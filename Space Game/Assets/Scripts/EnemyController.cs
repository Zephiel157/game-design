﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour {

	public GameObject Enemy;
	public AudioClip destroy;
	private int enemyRemaining;
	private int wave = 1;
	private int increaseWave = 1;
	public Text waveText;


	// Use this for initialization
	void Start () {

	}
		//Begins the game
		void BeginGame(){

			SpawnEnemy(); //spawns the Enemy into the game world
		}
		void OnTriggerEnter2D(Collider2D Collider){

			if (Collider.gameObject.tag.Equals ("BlueLaser"))
			Destroy (Collider.gameObject);
	
			if (Collider.gameObject.tag.Equals ("Enemy"))
			Destroy (Collider.gameObject);
	}
		void OnCollisionEnter2D(Collision2D c){

		if (c.gameObject.tag.Equals ("BlueLaser")) {

			// Destroy the bullet
			Destroy (c.gameObject);

			GameObject.Destroy (Enemy); 
		}
		}
	
		void SpawnEnemy(){

			DestroyExistingEnemy(); //destroys still existing Enemy

			// Decide how many enemies to spawn
			// If any asteroids left over from previous game, subtract them
			enemyRemaining = (wave * increaseWave);

			for (int i = 0; i < enemyRemaining; i++) {

				// Spawn an asteroid
				Instantiate(Enemy,
					new Vector3(Random.Range(-12.0f, 12.0f),
						Random.Range(-13.0f, 13.0f), 0),
					Quaternion.Euler(0,0,Random.Range(-0.0f, 359.0f)));

			}

			waveText.text = "WAVE: " + wave;
		}

	void DestroyExistingEnemy(){
		GameObject[] Enemy =
			GameObject.FindGameObjectsWithTag("Enemy");

		foreach (GameObject current in Enemy) {
			GameObject.Destroy (current);
		}
			
	}
}