﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Public Class for the MoveShip script
public class MoveShip : MonoBehaviour {

	private Transform tf; // This creates a variable for tf or our transform component
	public float turnspeed; //The physics variable for turning the gameobject
	public float speed; //The physics for speed for the gameobject
	private Rigidbody2D rb2d; //The physics for the 2D body

	// Use this for initialization
	void Start () {

		tf = GetComponent<Transform> (); //This loads the component into the variable

		
	}

	// Update is called once per frame
	void Update () {
		//if statement for the UpArrow key
		if (Input.GetKey (KeyCode.UpArrow)) {
			//UpArrow key transforms the position of the 2D sprite forward
			tf.position += tf.right * speed;

		}
		//if statement for the RightArrow key
		if (Input.GetKey (KeyCode.RightArrow)){
			//RightArrow key transforms the position of the 2D sprite to the right
			tf.Rotate (0, 0, -turnspeed);
	}	//if statement for the LeftArrow key
		if (Input.GetKey (KeyCode.LeftArrow)) {
			//LeftArrow key transforms the position of the 2D sprite to the left
			tf.Rotate (0, 0, turnspeed);

		}

	}
}
