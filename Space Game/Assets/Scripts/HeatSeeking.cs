﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatSeeking : MonoBehaviour {

	public Transform targetTF;
	private Transform tf;
	public float speed;
	public bool isAlwaysSeeking;
	private Vector3 movementVector;
	public bool isDirectional;


	// Use this for initialization
	void Start () {
		tf = GetComponent <Transform> ();

		if (GameController.game.Ship != null) {
			targetTF = GameController.game.Ship.GetComponent<Transform> ();

			movementVector = targetTF.position - tf.position;
		}	
	}
	
	// Update is called once per frame
	void Update () {
		if (GameController.game.Ship != null) {
			targetTF = GameController.game.Ship.GetComponent<Transform> ();

		if (isAlwaysSeeking) {
			
			Vector3 movementVector;
			movementVector = targetTF.position - tf.position; //end position - start position

		}


		movementVector.Normalize(); //make it a length of 1
		movementVector = movementVector * speed;//make it a length of speed
		tf.position = tf.position + movementVector;

			if (isDirectional) {

				tf.right = movementVector;

			}
		}
		
	}
}
