﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour {
	//The public game object for the blue laser
	public GameObject BlueLaser;
	//audio clip for destruction
	public AudioClip crash;
	//gamecontroller
	private GameController gameController;

	// Use this for initialization
	void Start () {
		//Declaring gameobject for player ship
		GameController.game.Ship = this.gameObject;

		// Get a reference to the game controller object and the script
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		//Grabbing the gamecontroller component.
		gameController = gameControllerObject.GetComponent <GameController>();
		
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D c){

		// Anything except a bullet is an asteroid
		if (c.gameObject.tag != "BlueLaser") {

			AudioSource.PlayClipAtPoint
			(crash, Camera.main.transform.position);

			// Move the ship to the centre of the screen
			transform.position = new Vector3 (0, 0, 0); 

			// Remove all velocity from the ship
			GetComponent<Rigidbody2D> ().
			velocity = new Vector3 (0, 0, 0);

			gameController.DecrementLives ();
		}
	}

}
