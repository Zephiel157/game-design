﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayableArea : MonoBehaviour {
	//a trigger collider used to create a playable area with
	//game object destroying walls

	private GameController gameController;

	void Start (){

		// Get a reference to the game controller object and the script
		GameObject gameControllerObject =
			GameObject.FindWithTag ("GameController");

		gameController =
			gameControllerObject.GetComponent <GameController>();
	}

	public void OnTriggerExit2D (Collider2D otherCollider) {
		//destroys all game objects that leave the area.
		Destroy (otherCollider.gameObject);

		gameController.DecrementAsteroids();

		}
}
