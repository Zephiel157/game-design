﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueLaser : MonoBehaviour {
	//Public transform variable tf
	public Transform tf;
	//setting the speed component
	public float speed;

	// Use this for initialization
	void Start () {
		//tf get transform component
		tf = GetComponent <Transform> ();
		
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
		
	}
	//setting the trigger for the blue laser
	public void OnTrigger2D (CapsuleCollider2D otherCollider){
		//destroying the game object on triggered collision
		Destroy (otherCollider.gameObject);
		Destroy (gameObject);

	}

	public void Move() {
		//adding the movement physics to the bluelaser
		tf.position += tf.right * speed;


	}
}
