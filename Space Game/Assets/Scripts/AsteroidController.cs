﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour {

	public AudioClip destroy;
	public GameObject SAsteroid;

	private GameController gameController;

	// Use this for initialization
	void Start () {

		// Get a reference to the game controller object and the script
		GameObject gameControllerObject =
			GameObject.FindWithTag ("GameController");

		gameController =
			gameControllerObject.GetComponent <GameController>();

		// Push the asteroid in the direction it is facing
		GetComponent<Rigidbody2D>()
			.AddForce(transform.up * Random.Range(-50.0f, 150.0f));

		// Give a random angular velocity/rotation
		GetComponent<Rigidbody2D>()
			.angularVelocity = Random.Range(-0.0f, 90.0f);

	}

	void OnCollisionEnter2D(Collision2D c){

		if (c.gameObject.tag.Equals("BlueLaser")) {

			// Destroy the bullet
			Destroy (gameObject);

				// Just a small asteroid destroyed
				gameController.DecrementAsteroids();
			}

			// Play a sound
			AudioSource.PlayClipAtPoint(
				destroy, Camera.main.transform.position);

			// Add to the score
			gameController.IncrementScore();

			// Destroy the current asteroid
			Destroy (gameObject);

		}

	}

