﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {


	public GameObject BlueLaserPrefab;
	public Transform shootPoint;
	public AudioClip shoot;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Space)) {

			Instantiate (BlueLaserPrefab, shootPoint.position, shootPoint.rotation);

			// Play a shoot sound
			AudioSource.PlayClipAtPoint (shoot, Camera.main.transform.position);

		}
		
	}
}
