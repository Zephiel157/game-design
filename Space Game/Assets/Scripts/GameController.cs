﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public static GameController game;

	//Public game objects for asteroid, ship, and enemy
	public GameObject SAsteroid;
	public GameObject Ship;
	public GameObject Enemy;
	//The private integers for the score, enemies, lives, and waves
	private int score;
	private int hiscore;
	private int asteroidsRemaining;
	private int enemyRemaining;
	private int lives; 
	private int wave;
	private int increaseEachWave = 3;
	private int increaseEnemyWave = 0;
	//The public Text code
	public Text scoreText;
	public Text livesText;
	public Text waveText;
	public Text hiscoreText;

	//Awake game state
	void Awake (){
		if (game == null){
			game = this;
		}
	}

	// Use this for initialization
	void Start () {
		//The text integer for the high score
		hiscore = PlayerPrefs.GetInt ("hiscore", 0);
		BeginGame (); //begins the game
		//This will reload the game for when the player loses all three
		Scene scene = SceneManager.GetActiveScene();
		
	}
	
	// Update is called once per frame
	void Update () {

		// Quit if player presses escape
		if (Input.GetKey("escape"))
			Application.Quit();
		
	}

	//To start the game/program
	void BeginGame(){
		//The starting score , lives, and wave
		score = 0; //starting score
		lives = 4; //starting lives
		wave = 1; //starting wave of enemies

		// Prepare the HUD
		scoreText.text = "SCORE:" + score;
		hiscoreText.text = "HISCORE: " + hiscore;
		livesText.text = "LIVES: " + lives;
		waveText.text = "WAVE: " + wave;

		SpawnAsteroids(); //spawns the asteroids into the game world
		SpawnEnemy(); //Spawns the enemy ship into the world
	}
	// void start spawn asteroid game objects
	void SpawnAsteroids(){

		// Decide how many asteroids to spawn
		// If any asteroids left over from previous game, subtract them
		asteroidsRemaining = GameObject.FindGameObjectsWithTag("SAsteroid");
		//calculating for i
		for (int i = 0; i < asteroidsRemaining; i++) {

			// Spawn an asteroid
			Instantiate(SAsteroid,
				new Vector3(Random.Range(-12.0f, 12.0f),
					Random.Range(-13.0f, 13.0f), 0),
				Quaternion.Euler(0,0,Random.Range(-0.0f, 359.0f)));

		}
		// Has player destroyed all asteroids?
		if (asteroidsRemaining < 1) {

			// Start next wave
			SpawnAsteroids();
			wave++;

		}


		//The text for the enemy wave
		waveText.text = "WAVE: " + wave;
	}
	//Spawn an enemy
	void SpawnEnemy(){
		// Decide how many asteroids to spawn
		// If any asteroids left over from previous game, subtract them
		enemyRemaining = (wave * increaseEnemyWave);
		//A random chance for enemy to spawn
		if(Random.value > 0.7) //%30 percent chance (1 - 0.7 is 0.3) 
		{

			// Spawn an Enemy
			Instantiate(Enemy,
				new Vector3(Random.Range(-12.0f, 12.0f),
					Random.Range(-13.0f, 13.0f), 0),
				Quaternion.Euler(0,0,Random.Range(-0.0f, 359.0f)));
	}

	}
		//Increasing the score
		public void IncrementScore(){
		score++;
		//Adding to the score text
		scoreText.text = "SCORE:" + score;
		//If statement for when the score becomes higher than the high score
		//Create new high score
		if (score > hiscore) {
			hiscore = score;
			hiscoreText.text = "HISCORE: " + hiscore;

			// Save the new hiscore
			PlayerPrefs.SetInt ("hiscore", hiscore);
		}
			
	}
	//Decrement lives from text upon death/ship destruction
	public void DecrementLives(){
		lives--;
		livesText.text = "LIVES: " + lives;

		// Has player run out of lives?
		if (lives < 1) {
			// Restart the game
			SceneManager.LoadScene("Gameplay");
			BeginGame();
		}
	}
	//Decrement asteroids
	public void DecrementAsteroids(){
		asteroidsRemaining--;
	}
	//Decrement enemy
	public void DecrementEnemy(){
			enemyRemaining--;
		}
	}

