﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyVision : MonoBehaviour {

		public float fieldOfView = 45.0f;

	public bool CanSee ( GameObject Player )
	{
		// We use the location of our target in a number of calculations - store it in a variable for easy access.
		Transform targetTf = Player.GetComponent<Transform>();
		Vector3 targetPosition = targetTf.position;

		// Find the vector from the agent to the target
		// We do this by subtracting "destination minus origin", so that "origin plus vector equals destination."
		Vector3 agentToTargetVector = targetPosition - transform.position;

		// Find the angle between the direction our agent is facing (forward in local space) and the vector to the target.
		float angleToTarget = Vector3.Angle (agentToTargetVector, transform.forward);

		// if that angle is less than our field of view
		if ( angleToTarget < fieldOfView )
		{
			// Raycast
			RaycastHit2D hitInfo = Physics2D.Raycast (targetTf.position, agentToTargetVector);

			// If the first object we hit is our target 
			if (hitInfo.collider.gameObject == Player) {
				// return true 
				//    -- note that this will exit out of the function, so anything after this functions like an else
				return true;
			}
		}
		//   -- note that because we returned true when we determined we could see the target, 
		//      this will only run if we hit nothing or if we hit something that is not our target.
		return false; 
	}

}
	
