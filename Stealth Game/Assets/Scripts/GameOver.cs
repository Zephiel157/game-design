﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //Allows the unity to manage scenes
//Game Over Script
public class GameOver : MonoBehaviour {
	//Serialized private string so that it can be edited
	[SerializeField] private string loadLevel;
	//when player collider2D collides or enters the collider of the enemy game object
	//It triggers the load scene function to game over
	void OnTriggerEnter2D(Collider2D other)
	{	//If the other collider that enters the enemy has the tag "Player"
		if (other.CompareTag("Player"))

		{	//Load the "Game Over" scene.
			Debug.Log ("GameOver!");
			SceneManager.LoadScene (loadLevel);
	}

}
}