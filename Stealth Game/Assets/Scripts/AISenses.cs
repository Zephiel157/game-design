﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISenses : MonoBehaviour {

	public float hearingModifier; // 0 is deaf, 1 is "normal" hearing, 2 is "twice normal"

	//Call this function when an A.I. actively tries to listen to an object
	public bool CanHear (GameObject target) {
		//Is that object capable of making noise?
		NoiseMaker noiseMaker = target.GetComponent<NoiseMaker>();
		if (noiseMaker == null) {
			return false;
		}
		//Is it making noise?
		if (noiseMaker.volume <= 0) {
			return false;
		}
		//Is the noise loud enough (In other words: Am I close enough)?
		//Get distance
		float distance = Vector3.Distance (GetComponent<Transform> ().position,
			                 target.GetComponent<Transform> ().position);

		//Calculate the modified required distance
		float modDistance = noiseMaker.volume * hearingModifier;

		//Check if close enough
		if (distance <= modDistance) {
			return true;
		}


		//otherwise
		return false;


	}

	public bool CanSee (GameObject target) {
		//Field of View
		Vector3 forwardVector = GetComponent<Transform>().forward;
		Vector3 vectorToTarget = target.GetComponent<Transform> ().position - GetComponent<Transform> ().position;

		float angleToTarget = Vector3.Angle (forwardVector, vectorToTarget);
		if (angleToTarget > fieldOfView) {
			return false;
		}

		//Line of Sight
		//Create variable to stor info about anything we hit with our ray.

		RaycastHit2D hitInfo;
		hitInfo = Physics2D.Raycast (GetComponent<Transform> ().position, 
										vectorToTarget,
			                       viewDistance);


		if (hitInfo == null) {
			return false;

		}

		//If I can see the object
		if (hitInfo.collider.gameObject == target) {

			return true;

		}



		//Otherwise
		return false;

	}

}*/
