﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIState : MonoBehaviour {

	public Transform[] patrolPoints;
	public float speed;
	Transform currentPatrolPoint;
	int currentPatrolIndex;

	public transform target;
	public float chaseRange;


	// Use this for initialization
	void Start () {

		currentPatrolIndex = 0;
		currentPatrolPoint = patrolPoints [currentPatrolIndex];


	}

	void Update () {

		float distanceToTarget = Vector3.Distance (transform.position, target.position);
		if (distanceToTarget < chaseRange) {

			Vector3 targetDir = target.position - transform.position;
			float angle = Mathf.Atan2 (targetDir.y, targetDir.x) * Mathf.Rad2Deg - 90f;
			Quaternion q = Quaternion.AngleAxis (angle, Vector3.forward);
			transform.rotation = Quaternion.RotateTowards (transform.rotation, q, 180);

			transform.Translate (Vector3.up * Time.deltaTime * speed);


		}

	}




	// Update is called once per frame
	void Update () {

		transform.Translate (Vector3.up * Time.deltaTime * speed);

		if (Vector3.Distance(transform.position, currentPatrolPoint.position)<.1f){


			if (currentPatrolIndex + 1 < patrolPoints.Length) {

				currentPatrolIndex++;

			}
			else {

				currentPatrolIndex = 0;
			}
			currentPatrolPoint = patrolPoints [currentPatrolIndex];
		}
		Vector3 patrolPointDir = currentPatrolPoint.position - transform.position;
		float angle = Mathf.Atan2 (patrolPointDir.y, patrolPointDir.x) * Mathf.Rad2Deg - 90f;

		Quaternion q = Quaternion.AngleAxis (angle, Vector3.forward);
		transform.rotation = Quaternion.RotateTowards (transform.rotation, q, 180f);


	}
}
*/