﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
//the public class for Enemy Pathing
public class EnemyPath : MonoBehaviour {
	//Defines the patrol points
	public Transform[] patrolPoints;
	public float speed; //Allows for GameObject movement
	Transform currentPatrolPoint; //Determines the current patrol point to move towards
	int currentPatrolIndex; //defines the integer for the current patrol point index
	//defines the target
	public Transform target;
	public float chaseRange;//runs the chase program


	// Use this for initialization
	void Start () {
		//Sets the starting current patrol index to zero
		currentPatrolIndex = 0;
		//defines the current patrol points to the index
		currentPatrolPoint = patrolPoints [currentPatrolIndex];


	}

	void Update () {
		//Determines the detection range/distance between enemy and the player before detection
		float distanceToTarget = Vector3.Distance (transform.position, target.position);
		//if statement for detection
		if (distanceToTarget < chaseRange) {
			//the program for chasing the player
			Vector3 targetDir = target.position - transform.position;
			float angle = Mathf.Atan2 (targetDir.y, targetDir.x) * Mathf.Rad2Deg - 90f;
			Quaternion q = Quaternion.AngleAxis (angle, Vector3.forward);
			transform.rotation = Quaternion.RotateTowards (transform.rotation, q, 180);
			//speed at which enemy pursues player
			transform.Translate (Vector3.up * Time.deltaTime * speed);


		}

	}




	// Update is called once per frame
	void FixedUpdate () {
		//Enemy speed
		transform.Translate (Vector3.up * Time.deltaTime * speed);
		//if statement if player moves outside of chase range
		if (Vector3.Distance(transform.position, currentPatrolPoint.position)<.1f){

			//if statement for range
			if (currentPatrolIndex + 1 < patrolPoints.Length) {

				currentPatrolIndex++;

			}
			else { //else statement
				//return to patrol route
				currentPatrolIndex = 0;
			}
			currentPatrolPoint = patrolPoints [currentPatrolIndex];
		}//determining which patrol point to return to.
		Vector3 patrolPointDir = currentPatrolPoint.position - transform.position;
		float angle = Mathf.Atan2 (patrolPointDir.y, patrolPointDir.x) * Mathf.Rad2Deg - 90f;

		Quaternion q = Quaternion.AngleAxis (angle, Vector3.forward);
		transform.rotation = Quaternion.RotateTowards (transform.rotation, q, 180f);


	}
}
