﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //Allows the unity to manage scenes
//Start the program
public class CanvasMenu : MonoBehaviour {
	//the public void for playing/starting the game
	public void PlayGame(){
		//Activates/retrieves the scene "StealthGame"
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);

	}
	//the public void for quitting the game
	public void QuitGame(){
		//A debug log to allow the programmer to view if program is running properly
		Debug.Log ("QUIT!");
		//Quits the game
		Application.Quit ();

	}

}
