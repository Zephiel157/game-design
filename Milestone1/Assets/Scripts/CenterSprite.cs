﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterSprite : MonoBehaviour {

	private Rigidbody2D rb2d; //Store a reference to the Rigidbody2D component required to use 2D Physics.

	// Use this for initialization
	void Start () {

		//Get and store a reference to the Rigidbody2D component so that we can access it.
		rb2d = GetComponent<Rigidbody2D> ();
		
	}
	
	// Update is called once per frame
	void Update () {

		// If statement on user input of the Space Key
		if (Input.GetKey (KeyCode.Space)) {
			// Transforms object position to the vector that is the center of the screen.
			transform.position = new Vector3(0.6105089f,0.4059979f,-0.09637324f);
		
}
	}
}
