﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Enable_DisableMovement : MonoBehaviour {

	MovingObject myScript;

	// Use this for initialization
	void Start () {
			
		myScript = gameObject.GetComponent<MovingObject> ();

	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.P)) {

			if (myScript.enabled)

				myScript.enabled = false; //disables the moving object script
			
		 	else 

				myScript.enabled = true; //enables the movingobject script
			
		}

	}
}
