﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUnit : MonoBehaviour {

	private Rigidbody2D rb2d; //Store a reference to the Rigidbody2D component required to use 2D Physics.

	// Use this for initialization
	void Start () {
		//Get and store a reference to the Rigidbody2D component so that we can access it.
		rb2d = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {

		//Movement down by 1 unit being bound to the LeftShift key and DownArrow
		//User key inputs for LeftShift key and DownArrow Keys
		if (Input.GetKey (KeyCode.LeftShift)) {

			if (Input.GetKeyDown (KeyCode.DownArrow)) {

				transform.position += Vector3.down;

			}

		}
		//Movement up by 1 unit being bound to the LeftShift key and UpArrow
		//User key inputs for LeftShift and UpArrow Keys
		if (Input.GetKey (KeyCode.LeftShift)) {

			if (Input.GetKeyDown (KeyCode.UpArrow)) {

				transform.position += Vector3.up;

			}

		}
		//Movement down by 1 unit being bound to the LeftShift key and RightArrow
		//User key inputs for LeftShift and RightArrow Keys
		if (Input.GetKey (KeyCode.LeftShift)) {

			if (Input.GetKeyDown (KeyCode.RightArrow)) {

				transform.position += Vector3.right;

			}

		}
		//Movement down by 1 unit being bound to the LeftShift key and LeftArrow
		//User key inputs for LeftShift and LeftArrow Keys
		if (Input.GetKey (KeyCode.LeftShift)) {

			if (Input.GetKeyDown (KeyCode.LeftArrow)) {

				transform.position += Vector3.left;

			}
				
		}
	}
}
