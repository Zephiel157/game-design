﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The public class for the C# script
public class QuitGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//User input to exit the game/program
		if (Input.GetKey (KeyCode.Escape)) {
			Application.Quit (); //quitting the application
		}

	}
}
